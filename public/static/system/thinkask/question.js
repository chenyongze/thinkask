$(function(){

//收藏
$('.t-collect').click(function(event) {
	var o={};
	o['item_id'] = $(this).attr('id');
	o['type']	 = $(this).attr('type');
	$.ajax({
		url: '/question/ajax/collect',
		type: 'post',
		dataType: 'json',
		data: o,
       beforeSend:function(){
        //加载层
        var index = layer.load(0, {shade: false}); //0代表加载的风格，支持0-2
       },
       success:function(d){

			if(d.code){
	 			var index = layer.load(0, {shade: false,time: 10}); //0代表加载的风格，支持0-2
				$('.t-collect').text(d.msg);
	       	}else{
	       		var index = layer.load(0, {shade: false,time: 10}); //0代表加载的风格，支持0-2
	            cmzAlert(d.msg,2)
	       	}
	}
	})
	
	
});
// 点赞
$('.t_zhan').click(function(event) {
	var o={};
		o['answer_id'] = $(this).attr('id');
		o['vote_value'] = $(this).attr('vote_value');
		$.ajax({
			url: '/question/ajax/zhan.html',
			type: 'post',
			dataType: 'json',
			data: o,
	       beforeSend:function(){
	        //加载层
	        var index = layer.load(0, {shade: false}); //0代表加载的风格，支持0-2
	       },
	       success:function(d){

				if(d.code){
		 			  window.location.reload();
		       	}else{
		       		var index = layer.load(0, {shade: false,time: 10}); //0代表加载的风格，支持0-2
		            cmzAlert(d.msg,2)
		       	}
		}
		})
});


// 感谢
$('.t_thank').click(function(event) {
	var o={};
		o['answer_id'] = $(this).attr('id');
		$.ajax({
			url: '/question/ajax/thank.html',
			type: 'post',
			dataType: 'json',
			data: o,
	       beforeSend:function(){
	        //加载层
	        var index = layer.load(0, {shade: false}); //0代表加载的风格，支持0-2
	       },
	       success:function(d){

				if(d.code){
		 			  window.location.reload();
		       	}else{
		       		var index = layer.load(0, {shade: false,time: 10}); //0代表加载的风格，支持0-2
		            cmzAlert(d.msg,2)
		       	}
		}
		})
});
$('.t_focus').click(function(){
		$.ajax({
			url: '/ucenter/ajax/focusUser',
			type: 'post',
			dataType: 'json',
			data:{
				uid:$('#paraid').val()
			},
			success:function(data){
				if(data.code==1){
					window.location.reload();
				}else{
					//var index = layer.load(0, {shade: false,time: 10}); //0代表加载的风格，支持0-2
					window.location.reload();

				}
			}
		})
	});
//举报
$('.t_accusation').click(function(event) {\
	alert('还没处理')
	return false;
	var o={};
		o['answer_id'] = $(this).attr('id');
	$.ajax({
			url: '/question/ajax/accusation',
			type: 'post',
			dataType: 'json',
			data:{
				// uid:$('#paraid').val()
			},
			success:function(data){
				if(data.code==1){
					window.location.reload();
				}else{
					//var index = layer.load(0, {shade: false,time: 10}); //0代表加载的风格，支持0-2
					window.location.reload();

				}
			}
		})
});

})