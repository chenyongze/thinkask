<?php
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */

namespace app\common\model;
use think\Db;
use think\Model;

class UserFocus extends Model
{
    protected $request;

	//关注用户
    public function focus($data){
        $data['add_time']=time();
        return  Db::name('user_follow')-> insertGetId($data);
    }
	//取消关注用户(当前直接删除对应数据)
	public function unfocus($data){
		return Db::name('user_follow')->where('friend_uid',$data['friend_uid'])->where('fans_uid',$data['fans_uid'])->delete();
	}
	//获取用户的关注状态
	public function get_focus_st($data){
		return Db::name('user_follow')->where('friend_uid',$data['friend_uid'])->where('fans_uid',$data['fans_uid'])->select();
	}
}