<?php
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\common\controller;
use think\Controller;
use think\Request;
use think\Session;
use think\Cookie;
use think\Hook;
use think\Route;
use think\Loader;
use think\Db;
use think\Cache;

class Base extends controller
{
 protected $request;
 protected $getbase;
 protected function _initialize()
    {
    //请求
	  $this->request = Request::instance();	
    $this->getbase = model('Base');
    }

  protected function vali($rule,$msg,$data){
    $validate = new Validate($rule, $msg);
    $result   = $validate->check($data);
    return $result;
  }
    /**
     * [arrToStr 取数组里面的值]
     * @param  [type] $array [数组]
     * @param  [type] $att   [字符]
     * @return [type]        [description]
     */
  protected function arrTstr($array,$att){
  		return  $array[$att];
  }
  /**
   * [getuid 获得用户的UID]
   * @return [type] [description]
   */
  protected function getuid(){
    return (int)Session::get('thinkask_uid');
  }
  /**
   * [getuserinfo 获得用户信息 ]
   * @return [type] [description]
   */
  protected function getuserinfo($field=""){
    $uid = $this->getuid()?$this->getuid():0;
    $where['uid'] = $uid; 
   $userinfo =  model('Base')->getone('users',['where'=>$where]);
   if($field){
    return $userinfo[$field];
   }else{
    return $userinfo;
   }
  }
  /**
   * [is_admin 是否为后台管理员]
   * @return boolean [description]
   */
  protected function is_admin(){
    if(!$this->getuid()) $this->error('您还没有登陆');
    if($this->getuserinfo('group_id')!=1) $this->error('您没有后台管理权限');
  }
  /**
   * [getgroup 当前用户的组]
   * @return [type] [description]
   */
  protected function getgroup(){
    $uid = $this->getuid()?$this->getuid():0;
    $where['uid'] = $uid; 
    $info =  model('Base')->getone('users',['where'=>$where]);
    return $info['group_id'];
  }
  /**
   * [powergroup 判断是否是后台管理员]
   * @return [type] [description]
   */
  protected function powergroup(){
    if($this->getgroup()!=1){
      $this->error('您没有操作权限');
    }

  }
  /**
   * [powerlogin 判断是否登陆]
   * @return [type] [description]
   */
  protected function powerlogin(){
    if(!$this->getuid()){
      $this->error('您还没有登陆','/');
    }
  }
  /**
   * [error2 没有倒计时的确误页面]
   * @param  [type] $message [description]
   * @return [type]          [description]
   */
  protected function error2($message){
   $this->assign('error',$message);
   echo  $this->fetch('system/error2');
   die;
  }
  /**
   * [success2 没有倒计时的正确页面]
   * @param  [type] $message [description]
   * @return [type]          [description]
   */
  protected function success2($message){
     $this->assign('message',$message);
   echo  $this->fetch('system/error2');
   die;
  }

  /**
   * [getempty 判断值是否为空]
   * @return [type] [description]
   */
  protected function panduanfield($array,$data){
    //信息处理
    if($array['action']=="write"){
        $msg = $array['zh-c']."为必填项";
    }elseif($array['action']=="coose"){
        $msg = "请先选择".$array['zh-c'];
    }
    // if($array)


    show($array);

  }

}
