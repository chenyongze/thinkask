<?php 
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\common\behavior;
use \think\Controller;
use \think\Request;
use \think\Session;
use \think\Cookie;
use \think\Hook;
use \think\Lang;
// echo "当前模块名称是" . $request->module();
// echo "当前控制器名称是" . $request->controller();
// echo "当前操作名称是" . $request->action();
class Power extends Controller
{
    protected   $request;
    public function run(&$params)
    {
        
       if($this->request->module()=="install") return ; 
        
        $this->request = Request::instance();
        //当前的模型  
        $module = strtolower($this->request->module());
        //方法
        $action = strtolower($this->request->action());
        //控制器
        $controller = strtolower($this->request->controller());
        if($module=="index"&&$action=="index"&&$controller=="index"){
             //检测安装
            if (!is_file(ROOT_PATH . 'public/data' . DS . 'install.lock')) {
                header('Location: ' . url('install/index/index'));
                exit();
            }
        }
        //安装页面不检测SESSION
        if($module!="install"){
             $this->checkSession();
        }
       
        $this->assign('module',$module);
        $this->assign('action',$action);
        $this->assign('controller',$controller);
        if($module=="admin"){
            if(!session('thinkask_uid')){
                $this->error('您没有后台操作权限',url('ucenter/user/login'));
            }
        }
        

    }
    private function checkSession(){
    	if(session('thinkask_uid')){
            $userinfo = model('users')->getUserByUid(Session::get('thinkask_uid'));
            //给插入一个默认的图相
            if(!$userinfo['avatar_file']){
                $re = model('base')->getedit('users',['where'=>['uid'=>$userinfo['uid']]],['avatar_file'=>"/static/images/default_avatar/default.jpg"]);
            }
            if($this->request->module()=="admin"&&$userinfo['group_id']!=1){
                $this->error('您没有操作权限');
            }
    	}else{
            $userinfo['uid'] = 0;
    	}
        
        $this->assign('userinfo',$userinfo);
    }


}