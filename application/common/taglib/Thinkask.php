<?php


/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\common\Taglib;

use think\template\TagLib;
// use app\common\controller\Base;
/**
 * CX标签库解析类
 * @category   Think
 * @package  Think
 * @subpackage  Driver.Taglib
 * @author    liu21st <liu21st@gmail.com>
 */
class Thinkask extends Taglib{
	// 标签定义
	protected $tags   =  array(
		// 标签定义： attr 属性列表 close 是否闭合（0 或者1 默认1） alias 标签别名 level 嵌套层次
		'nav'       => array('attr' => 'catid', 'close' => 1), //获取导航
		'list'      => array('attr' => 'table,where,order,limit,id,sql,field,key','level'=>3),//列表
		'doc'       => array('attr' => 'model,field,limit,id,field,key','level'=>3),
		'recom'     => array('attr' => 'doc_id,id'),
		'link'		=> array('attr' => 'type,limit' , 'close' => 1),//友情链接
		'prev'		=> array('attr' => 'id,cate' , 'close' => 1),//上一篇
		'next'		=> array('attr' => 'id,cate' , 'close' => 1),//下一篇
	);
	//导航
	/** {nav catid="1"}
          <li class="<?php if($controller==$v['controller']&&$action==$v['action']&&$module==$v['module']){ echo "active";} ?>"><a target="{$v.target}" href="{$v.url}"><i class="icon icon-index"></i> {$v.title}</a></li>
           {/nav}
     */
	public function tagnav($tag, $content){
		$catid =  empty($tag['catid'])?'1':$tag['catid'];
		// show($catid);
		$parse  = $parse   = '<?php ';
		$parse .= '$nav = model("base")->getall("nv_index",["where"=>["catid"=>'.$catid.',"status"=>1],"order"=>"sort desc,id desc"]);';
		$parse .= 'foreach ($nav as $key => $v) {';
		$parse .= '?>';
		$parse .= $content;
		$parse .= '<?php } ?>';
		return $parse;
	}


}