<?php
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\question\controller;
use app\common\controller\Base;

class Admin extends base
{
   public function lists(){
 	$this->assign('category',$category = model('Base')->getall('category'));
 	$where=[];
 	if(input(['keyword'])) $where['question_content']=['like','%'.trim(current($this->request->only(['keyword']))).'%'];;
 	if(input('category_id')) $where['category_id']=current($this->request->only(['category_id']));
 	if(input('user_name')) $where['user_name']=current($this->request->only(['user_name']));
    $this->assign('list',$re = model('Base')->getpages('question',['where'=>$where,'alias'=>'qu','join'=>[[config('database.prefix').'users u','u.uid=qu.published_uid']]]));
    if($where){
    	$this->assign('searchcount',count($re));

    }
    return $this->fetch('question/admin/lists');
  }

}
