<?php
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\Question\controller;
use app\common\controller\Base;
use think\console\Command;

class Ajax extends Base
{
	public function _initialize()
    {
        //用户是否登陆
        if (!$this->getuid() || !$this->request->isAJax()) {
            return;
        }
    }
    public function edit()
    {

        $id = (int)current($this->request->only(['id']));
        //字段判断
        if ($question_content = $this->request->only(['question_content'])) {
            if (empty($question_content['question_content'])) {
                $this->error('标题不能为空');
            }
        }
         //开启强制分组
         if(getset('must_category')=='Y'){
            if ($category_id = $this->request->only(['category_id'])) {
                if (empty($category_id['category_id']) || $category_id['category_id'] < 1) {
                    $this->error('请选择分类');
                }
            } 
         }
        
        $ardb = $this->request->param();
        $ardb['add_time'] = time();
        $ardb['category_id'] = $ardb['category_id'];
        $ardb['published_uid'] = $this->getuid();
        $ardb['question_content']=htmlspecialchars($ardb['question_content']);
        $ardb['question_detail']=htmlspecialchars($ardb['question_detail']);

        if ($id) {
            //修改
            model('Base')->getedit('question', ['where' => "question_id=$id"], $ardb);
            model('Base')->getedit('posts_index', ['where' => "post_id=$id"], ['update_time' => time()]);
            $this->success('操作成功', "/question/$id.html");
        } else {
            // 新加
            $id = model('Base')->getadd('question', $ardb);
            if ($id) {
                $data['post_id'] = $id;
                $data['post_type'] = "question";
                $data['add_time'] = $ardb['add_time'];
                $data['update_time'] = time();
                //是否匿名
                $data['uid'] = $this->getuid();
                model('Base')->getadd('posts_index', $data);
                $this->success('操作成功', "/question/$id.html");

            }
        }
    }
    /**
         * 保存评论
         */
     public function comment(){
            if ($this->request->isAJax()) {
                $comm = $this->request->param();

                $comm['message'] = $comm['message'];
                $comm['time'] = time();
                //获取用户ip
                $comm['ip'] = ip2long(fetch_ip());
                //获取用户uid
                $comm['uid'] = $this->getuid();
                //是否填写评论内容
                if (empty($comm['message'])) {
                    $this->error("您没有填写评论内容哦");
                }

               if (model('base')->getadd("answer_comments",$comm)){
                        $this->success('评论成功!');
                    }else{
                        $this->error('评论提交失败了,请稍后重试吧!');
                    }
            }
        }
        //回答
        public function answer(){
         if ($this->request->isAJax()) {
                        $comm = $this->request->param();
                        //获取问题id
                        $comm['question_id'] = (int)$comm['question_id'];
                        //获取评论内容
                        $comm['answer_content'] = $comm['message'];
                        $comm['add_time'] = time();
                        //获取用户ip
                        $comm['ip'] = ip2long(fetch_ip());
                        //获取用户uid
                        $comm['uid'] = $this->getuid();
                        //是否填写评论内容
                        if (empty($comm['answer_content'])) {
                            $this->error("您没有填写回答内容哦");
                        }
                        if (is_mobile()) {
                            $comm['mobile'] = 'mobile';
                        }
                        if (empty($comm['question_id'])) {
                            return;
                        }
                        if (model('base')->getadd("answer",$comm)) {
                            //回复+1
                            model('base')->getinc('question',['where'=>"question_id=".$comm['question_id']],'answer_count');
                            $this->success('回答成功!');
                        } else {
                            $this->error('回答提交失败了,请稍后重试吧!');
                        }
                    }
        }
      
		//临时注释
        //用户关注/取消问题
        public  function focus(){
            if ($this->request->isAJax()) {
                $data=$this->request->param();
                $data['question_id'] =(int)$data['question_id'];
                $data['uid'] = $this->getuid();
                $result = model('Focus')->get_focus_st($data);
                if (empty($result)) {
                    $id = model('Focus')->focus($data);
                    if ($id) {
                        $this->success('关注成功');
                    } else {
                        $this->error('关注失败');
                    }
                } else {
                    $status = model('Focus')->unfocus($data);
                    if ($status === 1) {
                        $this->success('取消关注成功');
                    } else {
                        $this->error('取消怎么失败了呢?');
                    }
                }
            }else{
                $this->error('非法请求');
            }
        }
        //浏览量加1
        public function views(){
             if ($this->request->isAJax()) {
                model("base")->getinc("question",["where"=>$this->request->only(['question_id'])],"view_count");
                echo "ok";

             }
        }
        /**
         * [collect 收藏]
         * @return [type] [description]
         */
        public function collect(){
          if ($this->request->isAJax()) {
                $data=$this->request->param();
                $data['time'] = time();
                $data['ip'] = ip2long(fetch_ip());
                $data['uid'] = $this->getuid();
                if(model('base')->getone("users_collect",["where"=>["item_id"=>$data['item_id'],"type"=>$data['type']]])){
                    $this->error('您已收藏，请不要重复收藏');
                }
                if(model('base')->getadd("users_collect",$data)){
                    $this->success("已收藏");
                }

          }  
        }

        //赞
        public function zhan(){
            if ($this->request->isAJax()) {
                 $data=$this->request->param();
                 $answerinfo = model('base')->getone("answer",["where"=>["answer_id"=>$data['answer_id']]]);
                 if(!$answerinfo) $this->error('此回答不存在');
                 if($answerinfo['uid']==$this->getuid()){
                    $this->error('您不能赞自已的回答!');
                 }
                 // 是否有赞过，赞过不能再赞
                 if(model('base')->getone("answer_vote",["where"=>["answer_id"=>$data['answer_id'],"vote_uid"=>$this->getuid()]])){
                    $this->error('您不能重复赞回答!');
                 }

                    $data['answer_id'] = $data['answer_id'];
                    $data['vote_value'] = $data['vote_value'];
                    $data['vote_uid']    = $this->getuid();
                    $data['add_time']    = time();
                if(model('base')->getadd("answer_vote",$data)){
                    $this->success("成功点赞");

                }else{
                    $this->error("点赞失败，晚点再试吧！");
                }
            }
        }
        //感谢
         public function thank(){
            if ($this->request->isAJax()) {
                 $data=$this->request->param();
                 $answerinfo = model('base')->getone("answer",["where"=>["answer_id"=>$data['answer_id']]]);
                 if(!$answerinfo) $this->error('此回答不存在');
                 if($answerinfo['uid']==$this->getuid()){
                    $this->error('您不能感谢自已!');
                 }
                 // 是否有感谢过
                 if(model('base')->getone("answer_thanks",["where"=>["answer_id"=>$data['answer_id'],"uid"=>$this->getuid()]])){
                    $this->error('您不能重复感谢!');
                 }

                    $data['answer_id'] = $data['answer_id'];
                    $data['uid']    = $this->getuid();
                    $data['user_name']    = $this->getuserinfo('user_name');
                    $data['time']    = time();
                if(model('base')->getadd("answer_thanks",$data)){
                    $this->success("成功点赞");

                }else{
                    $this->error("点赞失败，晚点再试吧！");
                }
            }
        }


}
