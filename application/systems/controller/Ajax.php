<?php
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */

namespace app\systems\controller;
use app\common\controller\Base;
class Ajax extends Base
{

    // public function _initialize() {
           
    // }
    /**
     * [delete 删除单个]
     * @return [type] [description]
     */
    public function delete(){
         if($this->request->isAJax()&&$this->getuid()>0){
            $data = $this->request->param();
            $wheres = explode("-", decode($data['where']));
            $where[$wheres[0]] =$wheres[1]; 

            model('Base')->getdel(decode($data['table']),['where'=>$where]);
            $this->success('删除成功');
        }
    }
    /**
     * 排序
     */
    //     <pre style='padding:10px;border-radius:5px;background:#F5F5F5;border:1px solid #aaa;font-size:14px;line-height
    // :18px;'>Array
    // (
    //     [table] => bNnmZYf5aMWT5hklZXg6f918e
    //     [data] => Array
    //         (
    //             [sort-1] => 5
    //             [sort-2] => 0
    //         )

    // )
    // </pre>
    public function sort(){
         if($this->request->isAJax()&&$this->getuid()>0){
            $data = $this->request->param();
            if($data){
                // show($data);
                if(is_array($data['data'])){
                    foreach ($data['data'] as $k => $v) {
                        $t = explode("-", $k);
                        $where = $data['where']."=".$t[1];
                        model('Base')->getedit(decode($data['table']),['where'=>$where],[$t[0]=>$v]);
                    }
                    $this->success('排序成功');
                }
            }else{
                $this->error('没有需要排序的数据');
            }
         }
    }
    /**
     * [delmore 删除多个，IDS]
     * @return [type] [description]
     */
    public function delmore(){
        if($this->request->isAJax()&&$this->getuid()>0){
           $data = $this->request->param();
            if(!$data[$data['where']]) $this->error('最少需要选择一个');
            if(!$data['where']) $this->error('没有指定key名，无法进行where删除');
           //$data['where']｛WHRER条件的名字｝
           if(is_array($data[$data['where']])){
                //传入多个ID时
                 model('base')->getquery('delete from '.config('database.prefix').decode($data['table']).' where '.$data['where'].' in('.implode(",", $data[$data['where']]).')');
           }else{
                //传入单个ID时
                $where[$data['where']] = $data[$data['where']];
                model('Base')->getdel(decode($data['table']),['where'=>$where]);
               
           }

            
            $this->success('删除成功');

        }

    }
    public function add(){
            if($this->request->isAJax()){
               $data = $this->request->param();
                if(model('Base')->getadd(decode($data['table']),$data)){
                    $this->success('添加成功',$data['returnurl']);
                }else{
                    $this->error('添加错误');
                } 
            }
            

        }
     public function edit(){
            if($this->request->isAJax()){
               $data = $this->request->param();
               $wheres = explode("-", decode($data['where']));
                $where[$wheres[0]] =$wheres[1]; 
                unset($data[$wheres[0]]);
                if(model('Base')->getedit(decode($data['table']),['where'=>$where],$data)!==flase){
                    $this->success('修改成功',$data['returnurl']);
                }else{
                    $this->error('添加错误,检查是否有修改内容');
                } 
            }
            
        }
    public function testmail(){
        $set = getset('mail_config');
        json(send_mail($set['testmail']));
     }
}
