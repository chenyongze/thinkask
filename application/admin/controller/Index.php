<?php
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\admin\controller;
use think\Controller;

class Index extends controller
{
  public function index(){
    $data = $this->request->param();
    $topmenuname = $data['topmenuname']?$data['topmenuname']:"home_action";
  	//菜单
  	$defaultmenu = config('adminmenu');
    // show($defaultmenu);
  	$Models = finddirfromdir(APP_PATH."../application");
  	$no_join = ['ajax','index','asset','common','post','ucenter','admin'];
    //模型
    if($topmenuname=="admin_content_model"){
      	foreach ($Models as $k => $v) {
      		$path = APP_PATH."../application/".$v."/menu.php";
      		if(!in_array($v, $no_join)&&file_exists($path)){
      			$menu= include($path);
      			$defaultmenu['adminmenu']['child'][]=$menu['adminmenu'];
      			
      		}
    	}
    }
    //插件
      if($topmenuname=="admin_plus_model"){
        
        // $Plus = finddirfromdir(APP_PATH."../plus");
        // if(is_array($Plus)){
        //   foreach ($Plus as $k => $v) {
        //     $path = APP_PATH."../plus/".$v."/config.php";
        //     if(file_exists($path)){
        //       $menu= include($path);
        //       if($menu['plusmenu']){
        //         $defaultmenu['plus']['child'][]=$menu['plusmenu'];
        //       }
              
        //     }

        //   }
        // }
      }
    //左侧菜单
    $leftmenu = $defaultmenu;
    foreach ($leftmenu as $k => $v) {
      if($topmenuname!=$v['name']){
        unset($leftmenu[$k]);
      }
    }
    $this->assign('topmenu',$defaultmenu);
    $this->assign('leftmenu',$leftmenu);
  	$this->assign('topmenuname',$topmenuname);

  	return $this->fetch('admin/index/index');
  }
  public function toMain(){
  	return $this->fetch('admin/index/tomains');
  }
}
