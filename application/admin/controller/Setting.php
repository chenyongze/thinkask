<?php
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\admin\controller;
use think\Controller;

class Setting extends controller
{
  protected  function _initialize(){
    $systemconfi = [];
    foreach (model('Setting')->systemSet() as $key => $v) {
      $systemconfi[$v['varname']]=unserialize($v['value']);;
    }
    // show($systemconfi);
    // show(($systemconfi['slave_mail_config']['server']));
   // if(is_array($systemconfi['slave_mail_config']['server'])){
   //   echo $systemconfi['slave_mail_config']['server']; 
   // }
    
     // die;
     $this->assign('config',$systemconfi);
  }
  public function set($status="web"){
    $this->assign('status',$status);
    //风格
    $this->assign('template',finddirfromdir(ROOT_PATH ."/template"));
  	return $this->fetch('admin/setting/set');
  }



}
