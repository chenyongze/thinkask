<?php
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\admin\controller;
use think\Controller;

class Nv extends controller
{
  public function index(){
  	$data = $this->request->param();
  	//当前分类ID查出相应的导航信息
  	if($data['catid']){
  		$catid = (int)$data['catid'];
  		$this->assign('catid',$catid);
  		//导航信息
  		$this->assign('nav_menu', $nav_menu = model('Base')->getall('nv_index',['order'=>'sort desc,id desc','where'=>['catid'=>$catid]]));
      // show($nav_menu);
  	}
  	//所有分类
  	$this->assign('cat',model('base')->getall('nv_index_cat'));
    // $this->assign('nav_menu', model('Base')->getall('nv_index',['order'=>'sort desc,id desc']));

    return $this->fetch('admin/nv/index');
  }
 public function edit(){
  //所有分类
  $this->assign('cats',model('base')->getall('nv_index_cat'));
 	$data = $this->request->param();
  //分配分类信息{从分类下面进去的新建导航}
  if($data['catid']){
    $this->assign('nav_menu',['catid'=>$data['catid']]);
  }
  //不ID下面的分类信息
 	if($data['id']){
 		$this->assign('nav_menu',model('Base')->getone('nv_index',['where'=>['id'=>$data['id']]]));
 	}
    
    return $this->fetch('admin/nv/edit');
  }
  //导航分类
  public function catlist(){

    $this->assign('cats',model('base')->getall('nv_index_cat'));

  return $this->fetch('admin/nv/catlist');
  }
  /**
   * [catedit 分组修改]
   * @return [type] [description]
   */
  public function catedit(){
    $data = $this->request->param();
    if($data['id']){
      $this->assign('cat_nav_menu',model('Base')->getone('nv_index_cat',['where'=>['id'=>$data['id']]]));
    }
    
    return $this->fetch('admin/nv/catedit');
  }
 
  
}
