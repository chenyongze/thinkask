<?php
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */

return [
    // 默认输出类型后台默认不加HTML
    // 'default_return_type'    => '',

    // +----------------------------------------------------------------------
    // | 模板设置
    // +----------------------------------------------------------------------

    
'adminmenu'               => [
    'home'=>[
            // 
            'name'=>'home_action',
            'ico'=>'fa-home',
            'url'=>'/admin/index/toMain',
            'child'=>[
            
            ],
        ],
    [
        // 全局设置
        'name'=>'admin_glob_set',
        'ico'=>'fa-cog',
        'url'=>'',
        'child'=>[
            [
            // 站点信息
            'name'=>'admin_web_set',
            'ico'=>'',
            'url'=>'/admin/setting/set',
            'url_extra'=>'?status=site',
                ],

            [
            // 注册访问
            'name'=>'admn_reg_view',
            'ico'=>'',
            'url'=>'/admin/setting/set',
            'url_extra'=>'?status=register',
                ],
            [
            // 站点功能
            'name'=>'admin_web_function',
            'ico'=>'',
            'url'=>'/admin/setting/set',
            'url_extra'=>'?status=functions',
                ],
            [
            // 问题设置
            'name'=>'admin_question_set',
            'ico'=>'',
            'url'=>'/admin/setting/set',
            'url_extra'=>'?status=question',
                ],
            
            [
            // 分类设置
            'name'=>'admin_category_set',
            'ico'=>'',
            'url'=>'/admin/category/index'
                ],
            
             [
            // 邮件设置
            'name'=>'admi_mail_set',
            'ico'=>'',
            'url'=>'/admin/setting/set',
            'url_extra'=>'?status=mail',
                ],
             [
            // 开放平台
            'name'=>'admin_open',
            'ico'=>'',
            'url'=>'/admin/setting/set',
            'url_extra'=>'?status=openid',
                ],
             [
            // 界面设置
            'name'=>'admin_visual_set',
            'ico'=>'',
            'url'=>'/admin/setting/set',
            'url_extra'=>'?status=interface',
                ],
            [
            // 导航设置
            'name'=>'admin_navigation_set',
            'ico'=>'',
            'url'=>'/admin/nv/index'
                ],
             [
            // 导航设置
            'name'=>'admin_navigation_cat',
            'ico'=>'',
            'url'=>'/admin/nv/catlist'
                ],
        ],
    ],
    'adminmenu'=>[
        // 
        'name'=>'admin_content_model',
        'ico'=>'fa-puzzle-piece',
        'url'=>'',
        'child'=>[
            [
                // 
                'name'=>'model_list',
                'ico'=>'',
                'url'=>'/admin/addons/index',
                'url_extra'=>'',
                ],
            
         
            
        ],
    ],
    'plus'=>[
        'name'=>'admin_plus_model',
        'ico'=>'fa-random',
        'url'=>'',
        'child'=>[
             [
            // 
            'name'=>'admin_plus_list',
            'ico'=>'',
            'url'=>'/admin/addons/index',
            'url_extra'=>'',
            ],
            [
            'name'=>'admin_hook_list',
            'ico'=>'',
            'url'=>'/admin/addons/hooks',
            'url_extra'=>'',
            ],
            ['name'=>'plus_caiji',
                'ico'=>'fa-hand-lizard-o',
                'url'=>'',
                'child'=>[
                     [
                    // 规则
                    'name'=>'plus_caiji_role',
                    'ico'=>'',
                    'url'=>'admin/caiji/index',
                    'url_extra'=>'',
                    ],

                ]
            ],
            ['name'=>'plus_creat_user',
                'ico'=>'fa-user-plus',
                'url'=>'',
                'child'=>[
                     [
                    // 规则
                    'name'=>'plus_creat_user',
                    'ico'=>'',
                    'url'=>'/admin/creatuser/creat',
                    'url_extra'=>'',
                    ],
                   
                ]
            ],
        ],
    ],
    [
        // 用户中心
        'name'=>'admin_user_center',
        'ico'=>'fa-group',
        'url'=>'',
        'child'=>[
            [
            // 用户列表
            'name'=>'admin_user_list',
            'ico'=>'',
            'url'=>'admin/user/index'
                ],
            [
            // 用户组
            'name'=>'admin_user_group',
            'ico'=>'',
            'url'=>'admin/user/group'
                ],
            
        ],
    ],
     [
        // 审核管理
        'name'=>'system_name',
        'ico'=>'fa-shirtsinbulk',
        'url'=>'',
        'child'=>[
            [
            // 数据备份
            'name'=>'datebase_backup',
            'ico'=>'',
            'url'=>'/admin/database/index/type/export'
            ],
            [
            // 数据恢复
            'name'=>'datebase_import',
            'ico'=>'',
            'url'=>'/admin/database/index/type/import'
            ],
          
        ],
    ],
     [
        // 审核管理
        'name'=>'thinask_store_shop',
        'ico'=>'fa-dropbox',
        'url'=>'',
        'child'=>[
            [
            // 数据备份
            'name'=>'thinask_shop_page',
            'ico'=>'',
            'url'=>'http://www.thinkask.cn/official/store/store_list'
            ]
          
        ],
    ],
 
],


];
