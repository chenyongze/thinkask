<?php
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */

namespace addons\Syslogin;
use app\common\controller\Addons;

/**
 * 系统环境信息插件
 * @author thinkphp
 */

class Syslogin extends Addons{

    public $info = array(
        'name'=>'Syslogin',
        'title'=>'第三方登录',
        'description'=>'第三方登录',
        'status'=>0,
        'author'=>'molong',
        'version'=>'0.1'
    );
    public function index_login(){
        $html = '<a type="button" class="btn btn-outline btn-default">默认</a>';
        echo $html
    }

    public function loginBottomAddon(){

    }

    public function install(){
    	return true;
    }

    public function uninstall(){
    	return true;
    }
}