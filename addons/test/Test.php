<?php
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace addons\test;
use app\common\controller\Addons;

/**
 * 系统环境信息插件
 * @author thinkphp
 */

class Test extends Addons {

	public $info = array(
		'name'        => 'Test',
		'title'       => '测试',
		'description' => '用于显示一些服务器的信息',
		'status'      => 1,
		'author'      => 'molong',
		'version'     => '0.1',
	);

	public function AdminIndex(){
	}

	public function test(){
		
	}

	public function install(){
		return true;
	}


	public function uninstall(){
		return true;
	}
}