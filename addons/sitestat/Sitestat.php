<?php
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace addons\sitestat;
use app\common\controller\Addons;

/**
 * 系统环境信息插件
 * @author thinkphp
 */

class Sitestat extends Addons{

    public $info = array(
        'name'=>'Sitestat',
        'title'=>'站点统计信息',
        'description'=>'统计站点的基础信息',
        'status'=>1,
        'author'=>'thinkphp',
        'version'=>'0.2'
    );

    public function install(){
        return true;
    }

    public function uninstall(){
        return true;
    }

    //实现的AdminIndex钩子方法
    public function AdminIndex($param){
        $config = $this->getConfig();
        $this->assign('addons_config', $config);
		// $map['status'] = array('egt',0);
		// $maps['is_read'] = array('eq',0);
        if($config['display']){
            $info['users']		=	db('users')->where($map)->count();
            $info['userall']		=	db('users')->count();
            // $info['action']		=	db('ActionLog')->where(array('create_time'=>array('gt',strtotime(date('Y-m-d')))))->count();
            $info['category']	=	db('category')->count();
            // $info['model']   =   db('Model')->count();
            $this->assign('info',$info);
            $this->template('index/info');
        }
    }
}